/**Copyright (C) 2019 Clerical Terrors

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as Discord from "discord.js";
import {createLogger, format, transports} from "winston";
import * as sqlite3 from "sqlite3";
import * as toml from "@iarna/toml";
import * as fs from "fs";
import * as crypto from "crypto";
import {Collection, Message, MessageEmbed, Role, TextChannel} from "discord.js";
import {adjectives, colors, Config, names, uniqueNamesGenerator} from "unique-names-generator";
import toMaterialStyle from "material-color-hash";
import { exit } from "process";

const sqlite = sqlite3.verbose();
const DB = new sqlite.Database("db.sqlite");
const {combine, timestamp, printf} = format;
const myFormat = printf((info: any) => {
    return `${info.timestamp} ${info.level}: ${info.message}`;
});
const logger = createLogger({
    format: combine(
        timestamp(),
        myFormat,
    ),
    transports: [
        new transports.File({ filename: "log/error.log", level: "error" }),
        new transports.File({ filename: "log/combined.log" }),
    ],
});
const NameConfig: Config ={
    dictionaries: [adjectives, colors, names],
    separator: " ",
    style: "capital"
};

interface IConfig {
    "token" : string,
    "outputchan" : string,
    "gamemaster": {
        "id" : string[] | string,
        "role" : string[] | string,
    },
    "prefix": string
}

let CONFIG: IConfig;
try {
   CONFIG = toml.parse(fs.readFileSync("config.toml", "utf8")) as unknown as IConfig;
} catch (e) {
    logger.error("Error parsing config file on line " + e.line +
        ", column " + e.column + ": " + e.message);
    process.exit(1);
}
let OUTPUTCHAN : TextChannel;
let linkedChannels = {};
let userAliases = {};

const discordClient = new Discord.Client();
discordClient.login(CONFIG.token).catch((err: Error) => logger.error("Error while attempting login to Discord\n###" + err.stack));
discordClient.on("warn", (info: string) => logger.warn(info));
discordClient.on("error", (err) => logger.error(err));

/**
 * checks if a salt exists and generates one if not
 * @return {string} - the salt as a string
 */
function getSalt() : string {
    try {
        if(fs.existsSync("salt")) {
            let salt : string = fs.readFileSync("salt", "utf8");
            if(salt.length < 14) {
                salt = crypto.randomBytes(Math.ceil(32/2)).toString('hex').slice(0,32);
                fs.writeFileSync("salt", salt, "utf8");
            }
            return salt;
        } else {
            const salt = crypto.randomBytes(Math.ceil(32/2)).toString('hex').slice(0,32);
            fs.writeFileSync("salt", salt, "utf8");
            return salt;
        }
    } catch (e) {
        logger.error(e.stack);
    }
}

const SALT = getSalt();

/**
 * Deletes all data from the database and resets the salt
 * @return {boolean} true if successful, false if not
 */
function RESET() : boolean {
    try {
        let salt = crypto.randomBytes(Math.ceil(32/2)).toString('hex').slice(0,32);
        fs.writeFileSync("salt", salt, "utf8");
        DB.run("DELETE FROM `names`;");
        DB.run("DELETE FROM `linked_channels`;");
        linkedChannels = {};
        userAliases = {};
        return true;
    } catch (e) {
        logger.error(e.stack);
        return false;
    }
}

/**
 * Links two channels by storing the association in the linkedChannels object and storing it as a record in the database
 * @param firstChan {TextChannel} - a Discordjs collection representing a channel
 * @param secondChan {TextChannel} - a Discordjs collection representing a channel
 */
function linkChannels(firstChan: TextChannel, secondChan: TextChannel) {
    const firstId : string = firstChan.id;
    const secondId: string = secondChan.id;
    linkedChannels[firstId] = secondChan;
    linkedChannels[secondId] = firstChan;
    const insert = DB.prepare("INSERT INTO linked_channels (idchan, idlinked) VALUES (?,?), (?,?)",
        [firstId, secondId, secondId, firstId],
        (err : Error|null)  => {if(err !== null) logger.error(err.stack);});
    insert.run((err: Error|null) => {if(err !== null) logger.error(err.stack);});
}

/**
 * Unlinks two channels by removing their entries from the linkedChannels object and from the database
 * @param firstChan {TextChannel} - a Discordjs collection representing a channel
 * @param secondChan {TextChannel} - a Discordjs collection representing a channel
 */
function unlinkChannels(firstChan: TextChannel, secondChan: TextChannel) {
    const firstId : string = firstChan.id;
    const secondId: string = secondChan.id;
    delete linkedChannels[firstId];
    delete linkedChannels[secondId];
    const insert = DB.prepare("DELETE FROM `linked_channels` WHERE idchan = ? OR idchan = ?;",
        [firstId, secondId],
        (err : Error|null)  => {if(err !== null) logger.error(err.stack);});
    insert.run((err: Error|null) => {if(err !== null) logger.error(err.stack);});
}

/**
 * A simple function that checks if the channels are already linked just by checking if either or both exist as keys in
 * the linkedChannels object
 * @param firstChan {TextChannel} - a Discordjs collection representing a channel
 * @param secondChan {TextChannel} - a Discordjs collection representing a channel
 */
function checkIfAlreadyLinked(firstChan: TextChannel, secondChan: TextChannel) : boolean {
    const firstId : string = firstChan.id;
    const secondId: string = secondChan.id;
    return (linkedChannels.hasOwnProperty(firstId) || linkedChannels.hasOwnProperty(secondId));
}

/**
 * A function to return a string indicating which channels are linked
 * @return {string} - a formatted list of linked channels
 */
function getChannelLinks() : string {
    let response : string = "";
    let done  = [];
    for(const key in linkedChannels) {
        if(!done.includes(key)) {
            response += "<#" + key +"> <=> <#" + linkedChannels[key].id +">\n";
            const channel = linkedChannels[key] as TextChannel;
            done.push(channel.id);
        }
    }
    if(response == "") {
        return "No channels have been linked"
    } else
    return "These channels are currently linked:\n" + response;
}

/**
 * Fetches the channel to which one channel is linked
 * @param chanID {string|boolean} the Discord snowflake ID of the channel for which the link needs to be called, or
 * `FALSE` in case no such link exists
 */
function getLinkedChannel(chanID: string) : TextChannel|boolean {
    if(chanID in linkedChannels) {
        return linkedChannels[chanID]
    } else {
        return false;
    }
}

/**
 * A simple function to generate a unique hash from a user ID using the generated salt
 * @param user {string} - the discord snowflake ID of the user
 * @return {string} - the hashed ID
 */
function UserHash(user: string) : string {
    const hash = crypto.createHmac("sha256", SALT);
    hash.update(user);
    return hash.digest('hex')
}

/**
 * creates a random name of three words for a user and stores it in the database
 * @param id {string} the Discord snowflake ID of the user
 * @return {string} - the randomly generated name for the user
 */
function createAlias(id : string) : string {
    const hash: string = UserHash(id);
    const randomName: string = uniqueNamesGenerator(NameConfig);
    userAliases[hash] = randomName;
    const insert = DB.prepare("INSERT INTO names (id, name) VALUES (?,?);", [hash, randomName],
        (err : Error|null)  => {if(err !== null) logger.error(err.stack);});
    insert.run((err: Error|null) => {if(err !== null) logger.error(err.stack);});
    return randomName;
}

/**
 * Check if a user has an alias, returns it if so, if not creates a new one and returns that
 * @param id {string} - the Discord snowflake ID of the user
 */
function getAlias(id : string) : string {
    const hash = UserHash(id);
    if(hash in userAliases) {
        return userAliases[hash];
    } else {
        return createAlias(id);
    }
}

/**
 * Checks if a user is a gamemaster by ID or by their Roles
 * @param mess {Message} - a Discordjs Message collection object
 * @return {boolean} - true if the user is a gamemaster, false if not
 */
function checkIfAuthorized(mess : Message) : boolean {
    const userID = mess.author.id;
    const roles : Collection<string, Role> = mess.member.roles.cache;
    const rolesArray : string[] = mess.member.roles.cache.keyArray();
    const gamemasterID = CONFIG.gamemaster.id;
    const gamemasterRole = CONFIG.gamemaster.role;

    if(Array.isArray(gamemasterID) && gamemasterID.includes(userID)) return true;

    if(gamemasterID == userID) return true;

    if(Array.isArray(gamemasterRole)) {
        const intersection = gamemasterRole.filter(x => rolesArray.includes(x));

        if(intersection.length > 0) return true;
    }
    return roles.find(r => r.id === gamemasterRole) !== undefined;
}

/**
 * Generates a Rich Embed reply to output in a linked channel
 * @param content {string} - the cleaned content of the message
 * @param userId {string} - the Discord Snowflake ID of the user sending the message
 * @return {RichEmbed} a Discordjs RichEmbed object containing the message to be relayed
 */
function createReply(content : string, userId : string) : MessageEmbed {
    const name = getAlias(userId);
    const hash = UserHash(userId);
    const color: any = toMaterialStyle(name, 500).backgroundColor;
    return new MessageEmbed()
        .setColor(color)
        .setAuthor("Sent by " + name, "https://i.imgur.com/4GmILT8.png")
        .setDescription(content)
        .setTimestamp()
        .setFooter('sender ID ' + hash);
}

/**
 * Creates a Rich Embed object to output in the Output channel to track messages being relayed
 * @param message {Message} - a Discordjs Message collection object of the original post
 * @param content {string} - the cleaned content of the message being relayed
 * @param linkedChan {TextChannel} - the Discord channel to which the message is to be relayed
 * @return {RichEmbed} - a Discordjs Rich Embed object containing the reply to be output in the channel
 */
function relayToOutputChan(message: Message, content: string, linkedChan: TextChannel) : MessageEmbed {
    const userId : string = message.author.id;
    const userTag : string = message.author.tag;
    const userAvatar = message.author.avatarURL as unknown as string;
    const name : string = getAlias(userId);
    const hash : string = UserHash(userId);
    const color: string = toMaterialStyle(userId, 500).backgroundColor;
    const originChannel : TextChannel = message.channel as TextChannel;
    const originChannelName : string = originChannel.name;
    const targetChannel : string = linkedChan.name;

    return new MessageEmbed()
        .setColor(color)
        .setAuthor(userTag, userAvatar)
        .setTitle("Message relayed:")
        .setDescription(content)
        .setTimestamp()
        .setURL(message.url)
        .addField("full message", message.cleanContent)
        .addField("Originator channel", originChannelName, true)
        .addField("Target Channel", targetChannel, true)
        .addField("Anonymous name", name, true)
        .addField("UserID hash", hash, true)
}

/**
 * Several startup functions which require the bot to be logged in before they can work
 */
discordClient.on("ready", () => {
    DB.each("SELECT * from `names`;", (err : Error|null, row : any) => {
        exit(1);
        userAliases[row.id] = row.name;
    });
    DB.each("SELECT * FROM `linked_channels`;", (err: Error|null, row: any) => {
        linkedChannels[row.idchan] = discordClient.channels.cache.get(row.idlinked);
    });
    const channel = discordClient.channels.cache.get(CONFIG.outputchan);
    OUTPUTCHAN = channel as TextChannel;
    console.log("Bot startup complete");
});

/**
 * The main event listeners
 */
discordClient.on("message", (message: Discord.Message) => {
    if (message.author.bot || message.channel.type === "dm") { return; }

    /**
     * All functions which are restricted to gamemasters go here
     */
    if(checkIfAuthorized(message)) {
        /**
         * Linking channels
         */
        const linkMatch = message.content.match(new RegExp("^" + CONFIG.prefix + "link <#(.*)> <#(.*)>$", "i"));
        if (linkMatch !== null && linkMatch.length > 1) {
            const firstChan = discordClient.channels.cache.get(linkMatch[1]) as TextChannel;
            const secondChan = discordClient.channels.cache.get(linkMatch[2]) as TextChannel;
            const linked = checkIfAlreadyLinked(firstChan, secondChan);
            if(!linked) {
                linkChannels(firstChan, secondChan);
                OUTPUTCHAN.send("Channels <#" + firstChan.id + "> and <#" + secondChan.id + "> linked");
            } else {
                OUTPUTCHAN.send("Channels <#" + firstChan.id + "> and/or <#" + secondChan.id + "> are already linked to " +
                    `each other or to other channels\n\nUse the \`${CONFIG.prefix}list\` command to see all linked channels`);
            }
            return;
        }

        /**
         * Unlinking channels
         */
        const unlinkMatch = message.content.match(new RegExp("^" + CONFIG.prefix + "unlink <#(.*)> <#(.*)>$", "i"));
        if (unlinkMatch !== null && unlinkMatch.length > 1) {
            const firstChan = discordClient.channels.cache.get(unlinkMatch[1]) as TextChannel;
            const secondChan = discordClient.channels.cache.get(unlinkMatch[2]) as TextChannel;
            unlinkChannels(firstChan, secondChan);
            OUTPUTCHAN.send("Channels <#" + firstChan.id + "> and <#" + secondChan.id + "> unlinked");
            return;
        }

        /**
         * Listing all channels
         */
        const listMatch = message.content.match(new RegExp("^" + CONFIG.prefix + "list$", "i"));
        if (listMatch !== null) {
            OUTPUTCHAN.send(getChannelLinks());
            return;
        }

        /**
         * The reset function
         */
        const resetMatch = message.content.match(new RegExp("^" + CONFIG.prefix + "reset$", "i"));
        if (resetMatch !== null) {
            message.reply("Are you sure you want to reset? This is irreversible, reply YES (all caps) if you wish " +
                "to confirm within 10 seconds, or anything else if you want to cancel");
            const filter = m => m.author.id === message.author.id;
            message.channel.awaitMessages(filter, {max: 1, time: 10000})
                .then((response) => {
                    if(response.first().content === "YES") {
                        if(RESET()) {
                            OUTPUTCHAN.send("Succesfully reset, enjoy your next game");
                        } else {
                            OUTPUTCHAN.send("It seems an error has occured and the reset has not gone through. " +
                                "Please consult the logs for details");
                        }
                    } else {
                        OUTPUTCHAN.send("Reset cancelled.")
                    }
                })
                .catch(err => logger.error(err));
            return;
        }
    }
    /**
     * The generic help command
     */
    const helpMatch = message.content.match(new RegExp("^" + CONFIG.prefix + "help", "i"));
    if (helpMatch !== null && helpMatch.length > 0) {
        if(checkIfAuthorized(message) && message.channel == OUTPUTCHAN) {
            OUTPUTCHAN.send("This bot uses the following commands, only gamemasters can use these:\n**" +
            CONFIG.prefix + "link [#channelname] [#channelname]**: links these two channels, order does not matter. " +
                "A channel can only be linked to one channel at a time.\n**" +
                CONFIG.prefix + "unlink [#channelname] [#channelname]**: unlinks these two channels.\n**" +
                CONFIG.prefix + "list**: shows a list of channels that are linked.\n**" +
                CONFIG.prefix + "reset**: deletes all linked channels, player names, and regenerates the salt for a new game");
            OUTPUTCHAN.send("All sent messages will be tracked in this channel.")
        }
        message.channel.send("Send messages between channels by putting them between curly braces \`{like this}\`, " +
            "only the part between curly braces will be sent");
        return;
    }

    /**
     * The actual relay event listener
     */
    const messMatch = message.cleanContent.match(new RegExp("{(.+)}", "is"));
    if (messMatch !== null) {
        const chanID = message.channel.id;
        const linkedChan = getLinkedChannel(chanID);
        if(!linkedChan) {
            message.channel.send("This channel doesn't appear to be linked to any other")
        } else {
            const targetChan = linkedChan as TextChannel;
            targetChan.send(createReply(messMatch[1], message.author.id))
                .then(() => {
                    message.react('✅');
                    OUTPUTCHAN.send(relayToOutputChan(message, messMatch[1], targetChan));
                })
                .catch((err: Error) => {
                    logger.error(err.stack);
                    OUTPUTCHAN.send("Error while trying to send a message: " + err.message);
                });

        }
    }
});
