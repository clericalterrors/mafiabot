# Mafia Game Relay Bot

Simple Relay bot intended for mafia games

---

## Requirements

* Node v10.* (I recommend getting the latest version and not the LTS one)   
* Sqlite (though the actual package may not even be necessary)

## Installation

1. Simply clone the repo, and run
```
npm install
```
2. change the name of `db.example.sqlite` to `db.sqlite` and `config.example.toml` to `config.toml` you need these files to be there or the bot won't run, it won't create them from scratch of fix them if you accidentally empty them so **do not delete them**
3. enter all the necessary info into `config.toml`, it should end up looking something like this   

```toml
key = "Your Discord bot key"
outputchan= "The channel where the bot is supposed to output the tracked messages"
gamemaster.id = ["the Discord snowflake ID(s) of you and maybe other game masters"]
gamemaster.role = ["the Discord snowflake ID(s) of the role(s) of the gamemasters in the server"]
prefix = "--" # whatever prefix you prefer for the bot, defaults to this one
```   
4. make sure node can write files in the directory you cloned, it needs this to be able to generate the salt file   

### Problems during install

If you have an outdated version of Node or don't have node-gyp your install might hiccup on Sqlite3 depending on your OS. You can try installing the Sqlite package for your distro if you're on Linux.

## Using the bot
After installing simply add the bot to your server as you would with any other bot.

**Be advised that the bot is not intended to support multiple servers at once.**

You can use the `--help` command to see a short list of gamemaster-only commands, regular server users can use this to get an explanation of how the relay system works.

### Commands
For this section I'm assuming the bot prefix is set to the default `--`   

#### `--help`
Displays the list of commands    

#### `--link`
Syntax: `--link [#channelname] [#channelname]`   
Links these two channels, order does not matter. **NB:** A channel can only be linked to one channel at a time.   
The link is also stored in the Sqlite database and thus all links persist after bot shutdown as long as the DB stays intact.   

#### `--unlink`
Syntax: `--unlink [#channelname] [#channelname]`   
Unlinks these two channels, the link is also removed from the database   

#### `--list`   
Syntax: `--list`   
Shows a list of channels that are linked.    

#### `--reset`
Syntax: `--reset`   
Deletes all data from memory and from the database and regenerates a salt for the hashing function. Intended to be used to 
reset the bot for a new game. Will ask for confirmation before doing this. The original requester
must answer `YES` (case sensitive) to confirm or anything else to cancel.   


### Relaying Messages
Send messages between channels by putting them between curly braces `{like this}` only the part between curly braces will be sent.   
The user will be given a randomized name and sidebar color for the message which persist in the Sqlite database. A unique hash will also be given
for the user to confirm it's the same user sending the messages.   
![Example screenshot of relayed message](https://i.imgur.com/Fk3gLNm.png)

A copy of the message will be sent to the output channel, along with info on who sent it. This is not stored in the database   
![Example screenshot of tracked output message](https://i.imgur.com/BlV6VGi.png)

## Known Issues:
_Linking a channel to itself works and makes it so I can't link it to any other channel_   
Simply use the unlink command to fix this situation