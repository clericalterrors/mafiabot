"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Discord = require("discord.js");
const winston_1 = require("winston");
const sqlite3 = require("sqlite3");
const toml = require("@iarna/toml");
const fs = require("fs");
const crypto = require("crypto");
const discord_js_1 = require("discord.js");
const unique_names_generator_1 = require("unique-names-generator");
const material_color_hash_1 = require("material-color-hash");
const process_1 = require("process");
const sqlite = sqlite3.verbose();
const DB = new sqlite.Database("db.sqlite");
const { combine, timestamp, printf } = winston_1.format;
const myFormat = printf((info) => {
    return `${info.timestamp} ${info.level}: ${info.message}`;
});
const logger = winston_1.createLogger({
    format: combine(timestamp(), myFormat),
    transports: [
        new winston_1.transports.File({ filename: "log/error.log", level: "error" }),
        new winston_1.transports.File({ filename: "log/combined.log" }),
    ],
});
const NameConfig = {
    dictionaries: [unique_names_generator_1.adjectives, unique_names_generator_1.colors, unique_names_generator_1.names],
    separator: " ",
    style: "capital"
};
let CONFIG;
try {
    CONFIG = toml.parse(fs.readFileSync("config.toml", "utf8"));
}
catch (e) {
    logger.error("Error parsing config file on line " + e.line +
        ", column " + e.column + ": " + e.message);
    process.exit(1);
}
let OUTPUTCHAN;
let linkedChannels = {};
let userAliases = {};
const discordClient = new Discord.Client();
discordClient.login(CONFIG.token).catch((err) => logger.error("Error while attempting login to Discord\n###" + err.stack));
discordClient.on("warn", (info) => logger.warn(info));
discordClient.on("error", (err) => logger.error(err));
function getSalt() {
    try {
        if (fs.existsSync("salt")) {
            let salt = fs.readFileSync("salt", "utf8");
            if (salt.length < 14) {
                salt = crypto.randomBytes(Math.ceil(32 / 2)).toString('hex').slice(0, 32);
                fs.writeFileSync("salt", salt, "utf8");
            }
            return salt;
        }
        else {
            const salt = crypto.randomBytes(Math.ceil(32 / 2)).toString('hex').slice(0, 32);
            fs.writeFileSync("salt", salt, "utf8");
            return salt;
        }
    }
    catch (e) {
        logger.error(e.stack);
    }
}
const SALT = getSalt();
function RESET() {
    try {
        let salt = crypto.randomBytes(Math.ceil(32 / 2)).toString('hex').slice(0, 32);
        fs.writeFileSync("salt", salt, "utf8");
        DB.run("DELETE FROM `names`;");
        DB.run("DELETE FROM `linked_channels`;");
        linkedChannels = {};
        userAliases = {};
        return true;
    }
    catch (e) {
        logger.error(e.stack);
        return false;
    }
}
function linkChannels(firstChan, secondChan) {
    const firstId = firstChan.id;
    const secondId = secondChan.id;
    linkedChannels[firstId] = secondChan;
    linkedChannels[secondId] = firstChan;
    const insert = DB.prepare("INSERT INTO linked_channels (idchan, idlinked) VALUES (?,?), (?,?)", [firstId, secondId, secondId, firstId], (err) => { if (err !== null)
        logger.error(err.stack); });
    insert.run((err) => { if (err !== null)
        logger.error(err.stack); });
}
function unlinkChannels(firstChan, secondChan) {
    const firstId = firstChan.id;
    const secondId = secondChan.id;
    delete linkedChannels[firstId];
    delete linkedChannels[secondId];
    const insert = DB.prepare("DELETE FROM `linked_channels` WHERE idchan = ? OR idchan = ?;", [firstId, secondId], (err) => { if (err !== null)
        logger.error(err.stack); });
    insert.run((err) => { if (err !== null)
        logger.error(err.stack); });
}
function checkIfAlreadyLinked(firstChan, secondChan) {
    const firstId = firstChan.id;
    const secondId = secondChan.id;
    return (linkedChannels.hasOwnProperty(firstId) || linkedChannels.hasOwnProperty(secondId));
}
function getChannelLinks() {
    let response = "";
    let done = [];
    for (const key in linkedChannels) {
        if (!done.includes(key)) {
            response += "<#" + key + "> <=> <#" + linkedChannels[key].id + ">\n";
            const channel = linkedChannels[key];
            done.push(channel.id);
        }
    }
    if (response == "") {
        return "No channels have been linked";
    }
    else
        return "These channels are currently linked:\n" + response;
}
function getLinkedChannel(chanID) {
    if (chanID in linkedChannels) {
        return linkedChannels[chanID];
    }
    else {
        return false;
    }
}
function UserHash(user) {
    const hash = crypto.createHmac("sha256", SALT);
    hash.update(user);
    return hash.digest('hex');
}
function createAlias(id) {
    const hash = UserHash(id);
    const randomName = unique_names_generator_1.uniqueNamesGenerator(NameConfig);
    userAliases[hash] = randomName;
    const insert = DB.prepare("INSERT INTO names (id, name) VALUES (?,?);", [hash, randomName], (err) => { if (err !== null)
        logger.error(err.stack); });
    insert.run((err) => { if (err !== null)
        logger.error(err.stack); });
    return randomName;
}
function getAlias(id) {
    const hash = UserHash(id);
    if (hash in userAliases) {
        return userAliases[hash];
    }
    else {
        return createAlias(id);
    }
}
function checkIfAuthorized(mess) {
    const userID = mess.author.id;
    const roles = mess.member.roles.cache;
    const rolesArray = mess.member.roles.cache.keyArray();
    const gamemasterID = CONFIG.gamemaster.id;
    const gamemasterRole = CONFIG.gamemaster.role;
    if (Array.isArray(gamemasterID) && gamemasterID.includes(userID))
        return true;
    if (gamemasterID == userID)
        return true;
    if (Array.isArray(gamemasterRole)) {
        const intersection = gamemasterRole.filter(x => rolesArray.includes(x));
        if (intersection.length > 0)
            return true;
    }
    return roles.find(r => r.id === gamemasterRole) !== undefined;
}
function createReply(content, userId) {
    const name = getAlias(userId);
    const hash = UserHash(userId);
    const color = material_color_hash_1.default(name, 500).backgroundColor;
    return new discord_js_1.MessageEmbed()
        .setColor(color)
        .setAuthor("Sent by " + name, "https://i.imgur.com/4GmILT8.png")
        .setDescription(content)
        .setTimestamp()
        .setFooter('sender ID ' + hash);
}
function relayToOutputChan(message, content, linkedChan) {
    const userId = message.author.id;
    const userTag = message.author.tag;
    const userAvatar = message.author.avatarURL;
    const name = getAlias(userId);
    const hash = UserHash(userId);
    const color = material_color_hash_1.default(userId, 500).backgroundColor;
    const originChannel = message.channel;
    const originChannelName = originChannel.name;
    const targetChannel = linkedChan.name;
    return new discord_js_1.MessageEmbed()
        .setColor(color)
        .setAuthor(userTag, userAvatar)
        .setTitle("Message relayed:")
        .setDescription(content)
        .setTimestamp()
        .setURL(message.url)
        .addField("full message", message.cleanContent)
        .addField("Originator channel", originChannelName, true)
        .addField("Target Channel", targetChannel, true)
        .addField("Anonymous name", name, true)
        .addField("UserID hash", hash, true);
}
discordClient.on("ready", () => {
    DB.each("SELECT * from `names`;", (err, row) => {
        process_1.exit(1);
        userAliases[row.id] = row.name;
    });
    DB.each("SELECT * FROM `linked_channels`;", (err, row) => {
        linkedChannels[row.idchan] = discordClient.channels.cache.get(row.idlinked);
    });
    const channel = discordClient.channels.cache.get(CONFIG.outputchan);
    OUTPUTCHAN = channel;
    console.log("Bot startup complete");
});
discordClient.on("message", (message) => {
    if (message.author.bot || message.channel.type === "dm") {
        return;
    }
    if (checkIfAuthorized(message)) {
        const linkMatch = message.content.match(new RegExp("^" + CONFIG.prefix + "link <#(.*)> <#(.*)>$", "i"));
        if (linkMatch !== null && linkMatch.length > 1) {
            const firstChan = discordClient.channels.cache.get(linkMatch[1]);
            const secondChan = discordClient.channels.cache.get(linkMatch[2]);
            const linked = checkIfAlreadyLinked(firstChan, secondChan);
            if (!linked) {
                linkChannels(firstChan, secondChan);
                OUTPUTCHAN.send("Channels <#" + firstChan.id + "> and <#" + secondChan.id + "> linked");
            }
            else {
                OUTPUTCHAN.send("Channels <#" + firstChan.id + "> and/or <#" + secondChan.id + "> are already linked to " +
                    `each other or to other channels\n\nUse the \`${CONFIG.prefix}list\` command to see all linked channels`);
            }
            return;
        }
        const unlinkMatch = message.content.match(new RegExp("^" + CONFIG.prefix + "unlink <#(.*)> <#(.*)>$", "i"));
        if (unlinkMatch !== null && unlinkMatch.length > 1) {
            const firstChan = discordClient.channels.cache.get(unlinkMatch[1]);
            const secondChan = discordClient.channels.cache.get(unlinkMatch[2]);
            unlinkChannels(firstChan, secondChan);
            OUTPUTCHAN.send("Channels <#" + firstChan.id + "> and <#" + secondChan.id + "> unlinked");
            return;
        }
        const listMatch = message.content.match(new RegExp("^" + CONFIG.prefix + "list$", "i"));
        if (listMatch !== null) {
            OUTPUTCHAN.send(getChannelLinks());
            return;
        }
        const resetMatch = message.content.match(new RegExp("^" + CONFIG.prefix + "reset$", "i"));
        if (resetMatch !== null) {
            message.reply("Are you sure you want to reset? This is irreversible, reply YES (all caps) if you wish " +
                "to confirm within 10 seconds, or anything else if you want to cancel");
            const filter = m => m.author.id === message.author.id;
            message.channel.awaitMessages(filter, { max: 1, time: 10000 })
                .then((response) => {
                if (response.first().content === "YES") {
                    if (RESET()) {
                        OUTPUTCHAN.send("Succesfully reset, enjoy your next game");
                    }
                    else {
                        OUTPUTCHAN.send("It seems an error has occured and the reset has not gone through. " +
                            "Please consult the logs for details");
                    }
                }
                else {
                    OUTPUTCHAN.send("Reset cancelled.");
                }
            })
                .catch(err => logger.error(err));
            return;
        }
    }
    const helpMatch = message.content.match(new RegExp("^" + CONFIG.prefix + "help", "i"));
    if (helpMatch !== null && helpMatch.length > 0) {
        if (checkIfAuthorized(message) && message.channel == OUTPUTCHAN) {
            OUTPUTCHAN.send("This bot uses the following commands, only gamemasters can use these:\n**" +
                CONFIG.prefix + "link [#channelname] [#channelname]**: links these two channels, order does not matter. " +
                "A channel can only be linked to one channel at a time.\n**" +
                CONFIG.prefix + "unlink [#channelname] [#channelname]**: unlinks these two channels.\n**" +
                CONFIG.prefix + "list**: shows a list of channels that are linked.\n**" +
                CONFIG.prefix + "reset**: deletes all linked channels, player names, and regenerates the salt for a new game");
            OUTPUTCHAN.send("All sent messages will be tracked in this channel.");
        }
        message.channel.send("Send messages between channels by putting them between curly braces \`{like this}\`, " +
            "only the part between curly braces will be sent");
        return;
    }
    const messMatch = message.cleanContent.match(new RegExp("{(.+)}", "is"));
    if (messMatch !== null) {
        const chanID = message.channel.id;
        const linkedChan = getLinkedChannel(chanID);
        if (!linkedChan) {
            message.channel.send("This channel doesn't appear to be linked to any other");
        }
        else {
            const targetChan = linkedChan;
            targetChan.send(createReply(messMatch[1], message.author.id))
                .then(() => {
                message.react('✅');
                OUTPUTCHAN.send(relayToOutputChan(message, messMatch[1], targetChan));
            })
                .catch((err) => {
                logger.error(err.stack);
                OUTPUTCHAN.send("Error while trying to send a message: " + err.message);
            });
        }
    }
});
